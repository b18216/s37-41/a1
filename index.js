// DEPENDENCIES
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
// SERVER
const app = express();
const port = 4000;

// DB CONNECTION
mongoose.connect("mongodb+srv://admin:admin@wdc028-course-booking.mvn65.mongodb.net/course-booking-182?retryWrites=true&w=majority"
, {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

let db = mongoose.connection;
db.on('error', console.error.bind(console, "connection error"));
db.once('open', () => console.log("Succesfully connected to the DataBase"));

// MIDDLEWARES
app.use(express.json());
app.use(express.urlencoded({
    extended: true
}));
app.use(cors());

// GROUP ROUTING
// connect files from routes folder
const userRoutes = require('./routes/userRoutes');
app.use('/users', userRoutes);

// connect course routes
const courseRoutes = require('./routes/courseRoutes');
app.use('/course', courseRoutes);

//PORT LISTENER
app.listen(port, () => console.log(`Server is Running at Port: ${port}`));
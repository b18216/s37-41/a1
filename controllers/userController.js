const User = require('../models/User');
const Course = require('../models/Course');
const bcrypt = require('bcryptjs');
const auth = require('../auth')

// USER REGISTER 
module.exports.registerUser = (req, res) => {
    console.log(req.body);
    // bcrypt.hashSync(string, saltRound)
    const hashedPW = bcrypt.hashSync(req.body.password, 10);

    let newUser = new User({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        mobileNo: req.body.mobileNo,
        password: hashedPW
    })
    // save new user to the DB
    newUser.save()
        .then(user => res.send(user))
        .catch(err => res.sent(err));
};


// Retrieval of ALL USERS
module.exports.getAllUsers = (req, res) => {
    User.find({})
        .then(result => res.send(result))
        .catch(err => res.send(err));
}


// LOG IN CONTROLLER
module.exports.loginUser = (req, res) => {
    console.log(req.body);

    User.findOne({
            email: req.body.email
        })
        .then(foundUser => {
            if (foundUser === null) {
                return res.send("No User Found");
            } else {
                const isPasswordCorrect = bcrypt.compareSync(req.body.password, foundUser.password)
                if (isPasswordCorrect) {
                    return res.send({
                        accessToken: auth.createAccessToken(foundUser)
                    })
                } else {
                    return res.send("Incorrect Password. Try Again")
                }

            };
        })
        .catch(err => res.send(err));
};
// GETTING SINGLE USER DETAIL
module.exports.getUserDetails = (req, res) => {
    User.findById(req.user.id)
        .then(result => res.send(result))
        .catch(err => res.send(err));
};

module.exports.checkEmailExist = (req, res) => {
    User.find({
            email: req.body.email
        })
        .then(result => {
            if (result === null) {
                res.send("Email is Available")
            } else {
                res.send("Email not Available")
            }
        })
        .catch(err => res.send(err));
};
// UPDATE USERS DETAILS
module.exports.updateUserDetails = (req, res) => {

    let updates = {
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        mobileNo: req.body.mobileNo
    }

    User.findByIdAndUpdate(req.user.id, updates, {
            new: true
        })
        .then(updatedUser => res.send(updatedUser))
        .catch(err => res.send(err));
}

// UPDATE USER TO ADMIN
module.exports.updateAdmin = (req, res) => {

    let updates = {
        isAdmin: true
    }
    User.findByIdAndUpdate(req.params.id, updates, {
            new: true
        })
        .then(updatedUser => res.send(updatedUser))
        .catch(err => res.send(err));
};

// ENROLLMENT
// async makes the function asynchronous

module.exports.enroll = async (req, res) => {

    if (req.user.isAdmin) {
        return req.send("You Are Not Allowed, Boss")
    }
    let isUserUpdated = await User.findById(req.user.id).then(user => {
        console.log(user);

        let newEnrollment = {
            courseId: req.body.courseId
        }
        user.enrollment.push(newEnrollment)
        return user.save()
            .then(user => true)
            .catch(err => err.messaage)
    })

    if (isUserUpdated !== true) {
        return res.send({
            message: isUserUpdated
        })
    }
    let isCourseUpdated = await Course.findById(req.body.courseId).then(course => {
        console.log(course)
        let enrollee = {
            userId: req.user.id
        }
        course.enrollees.push(enrollee)
        return course.save()
            .then(course => true)
            .catch(err => err.message);
    })

    if (isCourseUpdated !== true) {
        return res.send({
            message: isCourseUpdated
        });

    }
    if (isUserUpdated && isCourseUpdated) {
        return res.send({
            message: "User Enrolled Successfully"
        })
    }
}

// GET ENROLLMENTS
module.exports.getEnrollments =(req, res) => {
User.findById(req.user.id)
.then(result =>res.send(result.enrollments))
.catch(err => res.send(err));
}
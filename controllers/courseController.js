const Course = require('../models/Course')


// Course REGISTER
module.exports.registerCourse = (req, res) => {
    console.log(req.body);

    let newCourse = new Course({
        name: req.body.name,
        description: req.body.description,
        price: req.body.price

    });
    newCourse.save()
        .then(course => res.send(course))
        .catch(err => res.sent(err));
};

// Get All available Courses
module.exports.getAllCourses = (req, res) => {
    Course.find({})
        .then(result => res.send(result))
        .catch(err => res.send(err));
}
module.exports.getSingleCourse = (req, res) => {
    Course.findById(req.params.id)
        .then(result => res.send(result))
        .catch(err => res.send(err));
}
// Find course by id and Update
module.exports.updateCourse = (req, res) => {
    let updates = {
        name: req.body.name,
        description: req.body.description,
        price: req.body.price
    }

    Course.findByIdAndUpdate(req.params.id, updates, {
            new: true
        })
        .then(updatedCourse => res.send(updatedCourse))
        .catch(err => res.send(err));
}
// make course Inactive
module.exports.courseInactive = (req, res) => {

    let updates = {
        isActive: false
    }

    Course.findByIdAndUpdate(req.params.id, updates, {
            new: true
        })
        .then(inactive => res.send(inactive))
        .catch(err => res.send(err));

}

// make course active
module.exports.courseActive = (req, res) => {

    let updates = {
        isActive: true
    }

    Course.findByIdAndUpdate(req.params.id, updates, {
            new: true
        })
        .then(inactive => res.send(inactive))
        .catch(err => res.send(err));

}
// Select All Active Courses
module.exports.getActiveCourse = (req, res) => {
    Course.find({
            isActive: true
        })
        .then(result => res.send(result))
        .catch(err => res.send(err));
};

module.exports.findCourseByName = (req, res) => {
    Course.find({
            name: {
                $regex: req.body.name,
                $options: '$i'
            }
        })
        .then(result => {

            if (result.length === 0) {
                return res.send('No Courses Found')
            } else {
                return res.send(result)
            }
        })
}
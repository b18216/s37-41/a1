const mongoose = require('mongoose');
const userSchema = new mongoose.Schema({

    firstName: {
        type: String,
        required: [true, "User First Name is required"]
    },
    lastName: {
        type: String,
        required: [true, "User First Name is required"]
    },
    email: {
        type: String,
        required: [true, "Email is Required"]
    },
    password: {
        type: String,
        required: [true, "Password is required"]
    },
    mobileNo: {
        type: String,
        required: [true, "Email is required"]
    },
    isAdmin: {
        type: Boolean,
        default: false
    },
    enrollment: [{
        courseId: {
            type: String,
            required: [true, "Course is Required"]
        },
        status: {
            type: String,
            default: "Enrolled"
        },
        dateEnrolled: {
            type: Date,
            default: new Date()
        }
    }]

});
module.exports = mongoose.model("Users", userSchema);
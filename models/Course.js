const mongoose = require('mongoose');
const courseSchema = new mongoose.Schema({

    name: {
        type: String,
        required: [true, "Course Name is Required"]
    },
    description: {
        type: String,
        required: [true, "Course description is Required"]
    },
    price: {
        type: Number,
        required: [true, "Price is Required"]
    },

    isActive: {
        type: Boolean,
        default: true
    },
    createdOn: {
        type: Date,
        default: new Date()
    },
    enrollees: [{
        userId: {
            type: String,
            required: [true, "User ID is required"]
        },
        dateEnrolled: {
            type: Date,
            default: new Date()
        },
        status: {
            type: String,
            default: "Enrolled"
        }
    }]
});
module.exports = mongoose.model("Courses", courseSchema);
const jwt = require('jsonwebtoken');
const secret = "CourseBookingAPI";

module.exports.createAccessToken = (user) => {
    const data = {
        id: user._id,
        email: user.email,
        isAdmin: user.isAdmin
    };
    console.log(data);
    return jwt.sign(data, secret, {})
};

module.exports.verify = (req, res, next) => {
    // req.headers.authorization - contains jwt/token
    let token = req.headers.authorization;
    if (typeof token === "undefined") {
        return res.send({
            auth: "failed. No Token"
        })
    } else {
        console.log(token)
        token = token.slice(7, token.length)
        console.log(token);
        jwt.verify(token, secret, (err, decodedToken) => {
            if (err) {
                return res.send({
                    auth: "failed",
                    message: err.message
                })
            } else {
                req.user = decodedToken
                next();
            }

        });
    }
};


// VERIFYING IF ADMIN
module.exports.verifyAdmin = (req, res, next) => {
    if (req.user.isAdmin) {
        next()
    } else {
        return res.send({
            auth: "failed",
            message: "Action not Allowed"
        });
    }
};
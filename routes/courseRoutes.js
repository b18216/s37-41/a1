const express = require('express');
const router = express.Router();

const courseController = require('../controllers/courseController');
const Course = require('../models/Course');
const auth = require('../auth');
const {verify, verifyAdmin} = auth;

// route for registering new course
router.post('/', verify, verifyAdmin, courseController.registerCourse);
// route for getting all courses registered
router.get('/',courseController.getAllCourses);
// get single course by id
router.get('/getSingleCourse/:id',courseController.getSingleCourse);
// update course by id
router.put('/:id', verify, verifyAdmin, courseController.updateCourse);
// update course to inActive
router.put('/archieve/:id', verify, verifyAdmin, courseController.courseInactive);
// update course to Active
router.put('/activate/:id', verify, verifyAdmin, courseController.courseActive);
// select all Active course
router.get('/getAllActive/active', courseController.getActiveCourse);

router.get('/getCourses', courseController.findCourseByName);

module.exports=router;

const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');
const auth = require('../auth');
const {verify, verifyAdmin} = auth;

// ROUTES
// route for registering new User
router.post('/', userController.registerUser);

// Route for Getting all user
// getAllUser is taken from the controller file
router.get('/', userController.getAllUsers);

// Route for LOG IN
router.post('/login', userController.loginUser);

router.get('/getUserDetails', verify, userController.getUserDetails);

router.get('/checkEmailExist', userController.checkEmailExist);

router.put('/updatesUserDetails', verify, userController.updateUserDetails);

router.put('/updateAdmin/:id', verify, verifyAdmin, userController.updateAdmin )
// enables us to use route on index.js

router.post('/enroll', verify, userController.enroll);


router.get('/findEnrollment/:id', verify, userController.getEnrollments);



module.exports = router;


/*  
    "_id": "62984f7a42f82a04a538a49b",
    "firstName": "NewUser",
    "lastName": "Nolastname",
    "email": "samesample@gmail.com",
    "password":"testpassword",
    "mobileNo": "234234-2342"




    {
    "firstName": "User21",
    "lastName": "User21",
    "email": "samesample1@gmail.com",
    "password":"testpassword1",
    "mobileNo": "234234-2342"
}

*/